import React from 'react';
import _ from 'lodash';

export default ({
  label,
  value,
  disabled,
  onChange,
  type = 'text',
}) => {
  const id = _.uniqueId();

  return (
    <div className="input">
      {
        label &&
        <label htmlFor={id}>{label}</label>
      }
      <input
        className="input__input"
        id={id}
        type={type}
        disabled={disabled}
        value={value}
        onChange={onChange}
      />
    </div>
  );
}