import React from 'react';

export default ({
  name,
}) => {
  if (!name) {
    return null;
  }

  return (
    <div className="column sign">

      <div className="sign__content">
        {name}
      </div>
    </div>
  );
}