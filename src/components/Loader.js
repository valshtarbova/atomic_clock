import React from 'react';
import cx from 'classnames';

import Icon from 'components/Icon';

export default ({
  loading,
  children,
}) => {
  return (
    <div className="loader">
      {children}
      <div
        className={cx("loader__overlay", {
          "loader__overlay--loading": loading,
        })}
      >
        <Icon name="rotating_gears" />
      </div>
    </div>
  );
}