import React from 'react';
import _ from 'lodash';
import uuid from 'uuid';
import cx from 'classnames';

export default class Select extends React.Component {
  state = {
    query: '',
    selected: null,
    queryResult: [],
    hoveredOption: null,
  };

  id = uuid();

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
    this.input.addEventListener("keydown", this.handleKeyDown);

    const {
      value,
    } = this.props;

    this.setState({ query: this.selector(value), });
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
    this.input.removeEventListener("keydown", this.handleKeyDown);
  }

  componentDidUpdate(prevProps) {
    const {
      value,
    } = this.props;

    if (prevProps.value !== value) {
      this.setState({ query: this.selector(value), });
    }
  }

  selector = (value) => {
    const {
      selector = option => option,
    } = this.props;

    if (value) {
      return selector(value);
    }

    return "";
  }

  handleClickOutside = (event) => {
    if (this.element && !this.element.contains(event.target)) {
      this.onBlur();
    }
  }

  handleKeyDown = (e) => {
    const {
      hoveredOption,
      queryResult,
    } = this.state;
    if (e.keyCode === 40) {
      this.setState({
        hoveredOption: hoveredOption === null || hoveredOption >= queryResult.length - 1 ? 0 : hoveredOption + 1,
      });
    } else if (e.keyCode === 38) { //up
      this.setState({
        hoveredOption: hoveredOption === null || hoveredOption <= 0 ? queryResult.length - 1 : hoveredOption - 1,
      });
    } else if (e.keyCode === 13) {
      if (hoveredOption !== null) {
        e.preventDefault();
        this.selectOption(queryResult[hoveredOption]);
      }
    } else if (e.keyCode === 9) {
      this.onBlur();
    } else if (e.keyCode === 27) {
      this.onBlur();
    }
  }

  onBlur = () => {
    this.setState({
      queryResult: [],
      hoveredOption: null,
    });
  }

  queryChanged = (e) => {
    const {
      options = [],
      onChange,
      caseSensitive,
    } = this.props;
    const query = e.target.value;
    const queryResult = options.filter(option => {
      if (!caseSensitive) {
        return _.includes(this.selector(option).toLowerCase(), query.toLowerCase());
      }
      return _.includes(this.selector(option), query);
    }).slice(0, 5);

    this.setState({
      query: e.target.value,
      queryResult,
    });

    onChange && onChange({
      query: e.target.value,
      queryResult,
    });
  }

  selectOption = (option) => {
    const { onSelect, } = this.props;

    this.setState({
      query: this.selector(option),
      queryResult: [],
      hoveredOption: null,
    });

    onSelect && onSelect(option);
  }

  onFocus = (e) => {
    //should not trigger onChange, just show the list
    this.queryChanged(e);
  }

  render() {
    const {
      queryResult,
      query,
      hoveredOption,
    } = this.state;

    const {
      type = 'text',
      caseSensitive,
      label,
      disabled,
    } = this.props;

    return (
      <div ref={ref => this.element = ref} className="select">
        {
          label &&
          <label htmlFor={this.id}>{label}</label>
        }
        <input
          id={this.id}
          ref={ref => this.input = ref}
          disabled={disabled}
          type={type}
          className="select__input"
          value={query}
          onChange={this.queryChanged}
          onFocus={this.onFocus}
        />
        {
          _.size(queryResult) > 0 &&
          < div className="select__options">
            {
              queryResult.map((option, i) => {
                return (
                  <Option
                    key={_.uniqueId()}
                    className={cx('select__option', { 'select__option--hovered': i === hoveredOption })}
                    onClick={() => this.selectOption(option)}
                    option={this.selector(option)}
                    query={query}
                    caseSensitive={caseSensitive}
                  />
                );
              })
            }
          </div>
        }
      </div>
    );
  }
}

const Option = ({
  query,
  option,
  caseSensitive,
  className,
  onClick,
}) => {
  let index = option.indexOf(query);
  if (!caseSensitive) {
    index = option.toLowerCase().indexOf(query.toLowerCase());
  }
  const parts = index === - 1 ? [option] : [
    option.slice(0, index),
    option.slice(index, index + query.length),
    option.slice(index + query.length, option.length),
  ]
  return (
    <div className={className} onClick={onClick}>
      {parts[0]}
      <span className="select__option-highlight">
        {parts[1]}
      </span>
      {parts[2]}
    </div>
  );
}