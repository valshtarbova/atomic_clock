import React from 'react';
import cx from 'classnames';

export default ({
  onSubmit,
  children,
  className,
}) => {
  return (
    <form
      className={cx('form', className)}
      onSubmit={e=> {
        if (onSubmit) {
          e.preventDefault();
          onSubmit();
        }
      }}
    >
      {children}
      {
        onSubmit &&
        <input className="form__submit" type="submit" />
      }
    </form>
  );
}