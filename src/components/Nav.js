import React from 'react';
import _ from 'lodash';
import { Link, withRouter, } from "react-router-dom";
import cx from "classnames";

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { authActions } from 'redux/reducers/authReducer';

// Components
import Icon from 'components/Icon';

const Nav = ({
  user,
  isAuth,
  location,
}) => {
  if (!isAuth) {
    return null;
  }

  const path = location.pathname;

  return (
    <nav className="nav column row-reverse-gt-xs">
      <hr className="nav__hr" />
      <div className="nav__list row self-center-gt-xs column-gt-xs space-around flex-gt-xs" >
        <Link to="/dashboard"
          className={cx(
            "nav__link", {
              "nav__link--active": path === '/dashboard' || path === '/dashboard/',
            }
          )}
        >
          <Icon name="dashboard" className="nav__link-icon nav__link-icon nav__link-icon-dashboard" />
        </Link>

        <Link to="/timer"
          className={cx(
            "nav__link", {
              "nav__link--active": path === '/timer' || path === '/timer/' || path === '/',
            }
          )}
        >
          <Icon name="clock" className="nav__link-icon nav__link-icon-clock" />
        </Link>

        <Link to="/settings"
          className={cx(
            "nav__link", {
              "nav__link--active": path === '/settings' || path === '/settings/',
            }
          )}
        >
          <Icon className="nav__link-icon nav__link-icon-gear" name="gear" />
          <img className="nav__link-profile-img" src={_.get(user, 'photoURL')} alt="google profile" />
        </Link>
      </div>
    </nav>
  );
};

export default withRouter(connect(
  state => {
    const {
      user,
      isAuth,
    } = state.auth;
    return {
      user,
      isAuth,
    };
  },
  dispatch => {
    return {
      actions: bindActionCreators(authActions, dispatch),
    };
  }
)(Nav));