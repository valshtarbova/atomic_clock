import React from 'react';
import _ from 'lodash';
import moment from 'moment';

import timer from 'utils/timer';

export default class Timer extends React.Component {
  state = {
    seconds: 0,
    minutes: 0,
    hours: 0,
  }

  componentDidMount() {
    this.tick();
  }

  componentDidUpdate(prevProps) {
    const { state, logs, } = this.props;
    if (prevProps.state !== timer.STATES.START && state === timer.STATES.START) {
      this.tick();
    }

    if (_.size(logs) > 0 && _.size(prevProps.logs) === 0) {
      this.tick();
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  tick = () => {
    const { state, onTick, logs } = this.props;
    const period = timer.getPeriod(timer.calcTime(logs));

    onTick && onTick(period);

    this.setState({
      seconds: period.seconds,
      minutes: period.minutes,
      hours: period.hours,
    });

    if (state === timer.STATES.START) {
      this.timeout = setTimeout(this.tick, 300);
    }
  }

  toggle = () => {
    const { state, onStart, onStop } = this.props;
    if (state === timer.STATES.START) {
      onStop && onStop();
    } else {
      onStart && onStart();
    }
  }

  start = () => {
    const { onStart } = this.props;
    onStart && onStart();
  }

  stop = () => {
    const { onStop } = this.props;
    onStop && onStop();
  }

  render() {
    const { hours, minutes, seconds, } = this.state;
    const { state, disabled, } = this.props;

    return (
      <section className="timer">

        <div className="timer__counter">
          {moment().set({
            hours,
            minutes: Math.floor(minutes),
            seconds: Math.floor(seconds),
          }).format('HH:mm:ss')}
        </div>

        <div className="row">
          <button
            className="timer__action"
            disabled={disabled || state === timer.STATES.START}
            onClick={this.start}
          >
            Start
          </button>
          <button
            className="timer__action"
            disabled={!state || state === timer.STATES.STOP}
            onClick={this.stop}
          >
            Stop
          </button>
        </div>
      </section >
    );
  }

}