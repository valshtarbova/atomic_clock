import React from 'react';
import _ from 'lodash';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  withRouter,
  Switch,
} from "react-router-dom";

// redux
import { connect } from 'react-redux'

// components
import Nav from "components/Nav";

// pages
import Auth from "pages/Auth";
import Timer from "pages/Timer";
import Dashboard from "pages/Dashboard";
import Project from "pages/Project";
import Settings from "pages/Settings";

export default () => {
  return (
    <Router>
      <div className="main-container column row-reverse-gt-xs">
        <Switch>
          <Route path="/auth" component={Auth} />
          <PrivateRoute path="/timer" component={Timer} />
          <PrivateRoute path="/dashboard" component={Dashboard} />
          <PrivateRoute path="/settings" component={Settings} />
          <PrivateRoute path="/project/:id" component={Project} />
          <PrivateRoute component={Timer} />
        </Switch>
        <Nav />
      </div>
    </Router>
  );
};

const PrivateRoute = withRouter(connect(
  state => {
    return {
      isAuth: state.auth.isAuth,
    };
  },
)(({
  isAuth,
  component: Component,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={
        props => {
          const redirect = _.includes(props.location.pathname, '/auth') ? '/dashboard' : props.location.pathname;
          return (
            isAuth ?
              <Component {...props} /> : <Redirect
                to={{
                  pathname: `/auth`,
                  state: { redirect }
                }}
              />
          );
        }
      }
    />
  );
}));
