export const REDUCERS = {
  USER: 'user',
  USERS: 'users',
  AUTH: 'auth',
  TIMER: 'timer',
  PROJECTS: 'projects',
  TASKS: 'tasks',
  PROJECT_STATUS: 'projectStatus',
  CLIENTS: 'clients',
  PERMISSIONS: 'permissions',
  PERMISSIONS_GROUPS: 'permissionsGroups',
};