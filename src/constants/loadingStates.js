export const LOADINGSTATES = {
  INIT: 'INIT',
  LOADING: 'LOADING',
  LOADED: 'LOADED',
  ERROR: 'ERROR',
};

export const isLoading = (...states) => {
  for (const state of states) {
    if (state === LOADINGSTATES.LOADING) {
      return true;
    }
  }
  return false;
}

export const isLoadingAll = (...states) => {
  for (const state of states) {
    if (state !== LOADINGSTATES.LOADING) {
      return false;
    }
  }
  return true;
}

export const isLoaded = state => {
  return state === LOADINGSTATES.LOADED;
}