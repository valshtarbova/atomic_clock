import React, { Component } from 'react';
import 'App.css';

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { authActions } from 'redux/reducers/authReducer';
import { remindersActions } from 'redux/reducers/remindersReducer';

// components
import Router from 'Router';

class App extends Component {
  componentDidMount() {
    const { actions, } = this.props;
    actions.authUser();
  }

  componentDidUpdate(prevProps) {
    const {
      remindersActions,
      isAuth,
    } = this.props;

    if (isAuth && !prevProps.isAuth) {
      remindersActions.getReminders();
    }

  }

  render() {
    return (
      <div className="App">
        <header>

        </header>

        <Router />

        <footer>

        </footer>
      </div>
    );
  }
}

export default connect(
  state => {
    const {
      isAuth,
    } = state.auth;

    return {
      isAuth,
    };
  },
  dispatch => {
    return {
      actions: bindActionCreators(authActions, dispatch),
      remindersActions: bindActionCreators(remindersActions, dispatch),
    };
  }
)(App);