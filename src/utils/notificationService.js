import _ from 'lodash';
import moment from 'moment';

import { remindersRef } from 'utils/firebaseService';

const notifications = {};
let timer = null;
export const subscribeNotification = notification => {
  handlePermissions(() => {
    notification.subscribed = true;
    notifications[notification.id] = notification;

    if (!timer) {
      tick();
    }
  });
}

export const unsubscribeNotification = notification => {
  delete notifications[notification.id];
}

function tick() {
  _.forEach(notifications, (notification) => {
    if (moment(notification.date).diff(moment()) < 0) {
      // show again tomorrow
      notification.date = moment(
        moment(notification.date).format('HH:mm'), 'HH:mm'
      ).add(1, 'day').toDate();
      // update reminder
      remindersRef.doc(notification.id).set(notification).then(() => {
        // notify
        new Notification(notification.name);
      });
    }
  });

  timer = setTimeout(tick, 60000);
}


function handlePermissions(next) {
  if (!("Notification" in window) || Notification.permission === "denied") {
    return;
  }

  if (Notification.permission === "granted") {
    next();
  }

  Notification.requestPermission().then((permission) => {
    if (permission === "granted") {
      next();
    }
  });
}