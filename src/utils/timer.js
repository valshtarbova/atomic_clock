import moment from "moment";

export default {
  STATES: {
    STOP: "STOP",
    START: "START"
  },
  convertHoursToMinutes: function (hour) {
    const hours = hour.split(":")[0] * 1;
    const minutes = hour.split(":")[1] * 1;
    return minutes + hours * 60;
  },
  convertMinutesToHours: function (minutes) {
    const hours = Math.floor(minutes / 60);
    const _minutes = minutes % 60;
    return `${hours > 10 ? hours : '0' + hours}:${_minutes > 10 ? _minutes : '0' + _minutes}`;
  },
  //convert date to minutes(takes only days, hours and minutes into account)
  getMinutes: function (date) {
    return (
      moment(date).format("DDDD") * 24 * 60 +
      moment(date).format("HH") * 60 +
      moment(date).format("mm") * 1
    );
  },
  //convert date to seconds
  getSeconds: function (date) {
    return (
      moment(date).format("DDDD") * 24 * 60 * 60 +
      moment(date).format("HH") * 60 * 60 +
      moment(date).format("mm") * 60 +
      moment(date).format("ss") * 1
    );
  },
  format: function (seconds) {
    let days = Math.floor(seconds / 24 / 60 / 60);
    days = days > 0 ? days + 'd ' : '';

    let hours = Math.floor(seconds / 60 / 60 % 24);
    hours = hours > 0 ? hours + 'h ' : '';

    let minutes = Math.floor(seconds / 60 % 60);
    minutes = minutes > 0 ? minutes + 'm ' : '';

    return days + hours + minutes;
  },
  addPeriods: function (a, b) {
    let seconds = a.seconds + b.seconds;
    let minutes = a.minutes + b.minutes + Math.floor(seconds / 60);
    seconds = seconds % 60;
    const hours = a.hours + b.hours + Math.floor(minutes / 60);
    minutes = minutes % 60;
    return {
      seconds,
      minutes,
      hours,
    }
  },
  getPeriod: function (seconds) {
    return {
      seconds:  Math.floor(seconds % 60),
      minutes: Math.floor(seconds / 60 % 60),
      hours: Math.floor(seconds / 60 / 60)
    };
  },
  calcPeriod: function (newDate, oldDate) {
    return {
      seconds: Math.floor((newDate - oldDate) / 1000 % 60),
      minutes: Math.floor((newDate - oldDate) / 1000 / 60 % 60),
      hours: Math.floor((newDate - oldDate) / 1000 / 60 / 60)
    };
  },
  calcTime: function (arr) {
    if (!arr) {
      return 0;
    }
    let seconds = 0;
    arr.forEach((event, key) => {
      // if latest is run - calc up till now
      if (event.type === this.STATES.START && key === 0) {
        return seconds += this.getSeconds(moment().toDate()) - this.getSeconds(event.createdDate);
      }

      // if first is end - calc from beggining of day
      // not correct... should be the beggining of day from the day after the last start
      if (key + 1 === arr.length && event.type === this.STATES.STOP) {
        return seconds +=
          this.getSeconds(event.createdDate) -
          this.getSeconds(moment(event.createdDate).startOf("day").toDate());
      }

      if (event.type === this.STATES.START) {
        return seconds -= this.getSeconds(event.createdDate);
      }

      return seconds += this.getSeconds(event.createdDate);

    });

    return seconds;
  },
};