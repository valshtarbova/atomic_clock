
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const FirebaseConfig = {
  apiKey: "AIzaSyCi18TfXB2aPgf3qDrJjtZ0AxMuIVdpgAA",
  authDomain: "atomic-clock-37550.firebaseapp.com",
  projectId: "atomic-clock-37550",
  databaseURL: "https://atomic-clock-37550.firebaseio.com",
};

firebase.initializeApp(FirebaseConfig);

const database = firebase.firestore();

// const settings = {/* your settings... */ timestampsInSnapshots: true };
// database.settings(settings);

export const db = database;

export const clientsRef = database.collection("clients");
export const projectsRef = database.collection("projects");
export const projectStatusRef = database.collection("project_status");
export const remindersRef = database.collection("reminders");
export const tasksRef = database.collection("tasks");
export const timeLogsRef = database.collection("time_logs");
export const userLevelsRef = database.collection("user_levels");
export const usersRef = database.collection("users");
export const permissionsGroupsRef = database.collection("permissions_groups");

export default firebase;