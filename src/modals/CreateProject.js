import React, { Component } from 'react';
import _ from 'lodash';

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { projectStatusActions } from 'redux/reducers/projectStatusReducer';
import { projectsActions } from 'redux/reducers/projectsReducer';
import { clientsActions } from 'redux/reducers/clientsReducer';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';

// components
import Select from 'components/Select';
import Loader from 'components/Loader';
import Input from 'components/Input';
import Form from 'components/Form';

import Modal from 'modals/Modal';

class CreateProject extends Component {
  state = {
    name: '',
    status: null,
    client: null,
  }

  componentDidMount() {
    const { actions } = this.props;

    actions.clients.getClients();
    actions.projectStatus.getProjectStatus();
  }

  componentDidUpdate(prevProps) {
    const { createProjectState, onClose, } = this.props;
    if (createProjectState === LOADINGSTATES.LOADED && prevProps.createProjectState === LOADINGSTATES.LOADING) {
      this.setState({
        name: '',
        status: null,
        client: null,
      });
      onClose && onClose();
    }
  }

  createProject = () => {
    const { actions, } = this.props;
    const {
      name,
      client,
      status,
    } = this.state;

    actions.projects.createProject({ name, clientRef: client.ref, status: status.name });
  }

  onSelect = (key, value) => {
    this.setState({ [key]: value });
  }

  render() {
    const {
      createProjectState,
      clients,
      clientsState,
      projectStatus,
      projectStatusState,
      visible,
      onClose,
    } = this.props;

    const {
      name,
      client,
      status,
    } = this.state;
    const createBtnDisabled = !name || !status || !client;
    const inputsDisabled = createProjectState === LOADINGSTATES.LOADING;
    const loading = clientsState === LOADINGSTATES.LOADING || projectStatusState === LOADINGSTATES.LOADING;
    return (
      <Modal opened={visible} onClose={onClose}
        header="Create Project"
        actions={
          <div className="create-project__action">
            <Loader loading={createProjectState === LOADINGSTATES.LOADING}>
              <button disabled={createBtnDisabled} onClick={this.createProject} >Create</button>
            </Loader>
          </div>
        }
      >
        <Loader loading={loading}>
          <div className="create-project__content">
            <Form onSubmit={this.createProject}>
              <Input
                label="Name"
                disabled={inputsDisabled}
                value={name}
                onChange={e => this.setState({ name: e.target.value })}
              />

              <Select
                label="Client"
                value={_.get(client, 'name', '')}
                disabled={inputsDisabled}
                options={clients}
                selector={client => client.name}
                onSelect={client => this.onSelect('client', client)}
              />

              <Select
                label="Status"
                value={_.get(status, 'name', '')}
                disabled={inputsDisabled}
                options={projectStatus}
                selector={status => status.name}
                onSelect={status => this.onSelect('status', status)}
              />
            </Form>
          </div>
        </Loader>
      </Modal>
    );
  }
}

export default connect(
  state => {
    const {
      createProjectState,
    } = state.projects;
    const {
      clients,
      state: clientsState,
    } = state.clients;
    const {
      projectStatus,
      state: projectStatusState,
    } = state.projectStatus;
    return {
      createProjectState,
      clients,
      clientsState,
      projectStatus,
      projectStatusState,
    };
  },
  dispatch => {
    return {
      actions: {
        projects: bindActionCreators(projectsActions, dispatch),
        clients: bindActionCreators(clientsActions, dispatch),
        projectStatus: bindActionCreators(projectStatusActions, dispatch),
      }
    };
  }
)(CreateProject);