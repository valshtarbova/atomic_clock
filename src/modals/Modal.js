import React from 'react';
import cx from 'classnames';

export default ({
  opened,
  onClose,
  children,
  header,
  actions,
}) => {
  return (
    <div className={cx('modal', { 'modal--visible': opened })}>
      <div className="modal__backdrop" onClick={onClose}>
      </div>
      <div className={cx('modal__content', { 'modal__content--visible': opened })}>
        {opened &&
          header && <div className="modal__header">{header}</div>
        }
        {opened &&
          header && <hr />
        }

        {opened &&
          children
        }

        {opened &&
          actions && <hr />
        }
        {opened &&
          actions && <div className="modal__actions">{actions}</div>
        }
      </div>
    </div>
  );
}