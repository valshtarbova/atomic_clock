import React, { Component } from 'react';
import _ from 'lodash';
import uuid from 'uuid';
import moment from 'moment';

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { remindersActions } from 'redux/reducers/remindersReducer';

// constants
import { isLoading, isLoaded } from 'constants/loadingStates';

// components
import Loader from 'components/Loader';
import Input from 'components/Input';
import Form from 'components/Form';

import Modal from 'modals/Modal';

class SaveReminder extends Component {
  state = {
    name: '',
    date: moment().format('HH:mm'),
    id: uuid(),
  }

  componentDidUpdate(prevProps) {
    const { saveReminderState, onClose, reminder } = this.props;

    if (_.get(reminder, 'id') && _.get(reminder, 'id') !== _.get(prevProps.reminder, 'id')) {
      this.setState({
        name: reminder.name,
        date: moment(reminder.date).format('HH:mm'),
        id: reminder.id,
      });
    }

    if (isLoaded(saveReminderState) && isLoading(prevProps.saveReminderState)) {
      this.setState({
        name: '',
        date: moment().format('HH:mm'),
        id: uuid(),
      });
      onClose && onClose();
    }
  }

  saveReminder = () => {
    const { remindersActions, } = this.props;
    const {
      name,
      date,
      id,
    } = this.state;

    const datetime = moment(date, 'HH:mm').diff(moment()) > 0 ?
      moment(date, 'HH:mm') : moment(date, 'HH:mm').add(1, 'day');
    remindersActions.saveReminder({ name, date: datetime.toDate(), id, });
  }

  render() {
    const {
      saveReminderState,
      reminder,
      onClose,
    } = this.props;

    const {
      name,
      date,
    } = this.state;

    return (
      <Modal opened={!!reminder} onClose={onClose}
        header="Reminder"
        actions={
          <div className="row flex-center center">
            <Loader loading={isLoading(saveReminderState)}>
              <button disabled={!name} onClick={this.saveReminder}>Save</button>
            </Loader>
          </div>
        }
      >
        <div className="save-reminder__content">
          <Form onSubmit={this.saveReminder}>
            <Input
              label="Name"
              value={name}
              onChange={e => this.setState({ name: e.target.value })}
            />

            <Input
              type="time"
              label="Time"
              value={date}
              onChange={e => {
                this.setState({ date: e.target.value })
              }}
            />
          </Form>
        </div>
      </Modal>
    );
  }
}

export default connect(
  state => {
    const {
      saveReminderState,
    } = state.reminders;

    return {
      saveReminderState
    };
  },
  dispatch => {
    return {
      remindersActions: bindActionCreators(remindersActions, dispatch),
    };
  }
)(SaveReminder);