import React, { Component } from 'react';
import _ from 'lodash';
import uuid from 'uuid';
import moment from 'moment';

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { remindersActions } from 'redux/reducers/remindersReducer';

import SaveReminder from 'modals/SaveReminder';

// components
import Loader from 'components/Loader';
import Icon from 'components/Icon';

// constants
import { isLoading } from 'constants/loadingStates';

class Settings extends Component {
  state = {
    reminderObj: null,
  }

  componentDidMount() {
    const {
      remindersActions,
    } = this.props;

    remindersActions.getReminders();

    if ('permissions' in navigator) {
      navigator.permissions.query({ name: 'notifications' }).then((notificationPerm) => {
        notificationPerm.onchange = () => {
          this.forceUpdate();
        };
      });
    }
  }

  deleteReminder = (reminder) => {
    const {
      remindersActions,
    } = this.props;

    remindersActions.deleteReminder(reminder);
  }

  render() {
    const {
      reminders,
      remindersState,
    } = this.props;

    const {
      reminderObj,
    } = this.state;

    return (
      <section className="reminders">
        <Loader loading={isLoading(remindersState)}>
          <table>
            <caption>Reminders</caption>
            <thead>
              {
                ("Notification" in window) && Notification.permission === "granted" &&
                <tr>
                  <th>Email</th>
                  <th>Time</th>
                  <th>Status</th>
                  <th colSpan={2}>
                    <Icon
                      onClick={() => this.setState({
                        reminderObj: {
                          name: '',
                          date: moment().format(),
                          id: uuid(),
                        }
                      })}
                      className="reminders__icon" name="add_reminder"
                    />
                  </th>
                </tr>
              }
              {
                Notification.permission !== "granted" &&
                <tr><th>Notification are not allowed</th></tr>
              }
              {
                !("Notification" in window) &&
                <tr><th>Notification are not available</th></tr>
              }
            </thead>
            {
              ("Notification" in window) && Notification.permission === "granted" &&
              <tbody>
                {
                  _.map(reminders, (reminder) => {
                    return (
                      <tr key={_.uniqueId()}>
                        <td>{reminder.name}</td>
                        <td>{moment(reminder.date).format('HH:mm')}</td>
                        <td>
                          {
                            reminder.subscribed ?
                              <span className="notification__indicator notification__indicator--subscribed">
                              </span> : <span className="notification__indicator"></span>
                          }
                        </td>
                        <td>
                          <Icon
                            onClick={() => this.setState({ reminderObj: reminder })}
                            className="reminders__icon"
                            name="edit"
                          />
                        </td>
                        <td>
                          <Icon
                            onClick={() => this.deleteReminder(reminder)}
                            className="reminders__icon"
                            name="delete"
                          />
                        </td>
                      </tr>
                    );
                  })
                }
              </tbody>
            }
          </table>
        </Loader>

        <SaveReminder
          reminder={reminderObj}
          onClose={() => this.setState({ reminderObj: null, })}
        />

      </section>
    );
  }
}

export default connect(
  state => {
    const {
      reminders,
      remindersState,
    } = state.reminders;
    return {
      reminders,
      remindersState,
    };
  },
  dispatch => {
    return {
      remindersActions: bindActionCreators(remindersActions, dispatch),
    };
  }
)(Settings);