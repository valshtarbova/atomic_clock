import React, { Component } from 'react';
import _ from 'lodash';

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { authActions } from 'redux/reducers/authReducer';

// constants
import { isLoading } from 'constants/loadingStates';

// pages
import Reminders from 'pages/Reminders';

import Sign from 'components/Sign';

class Settings extends Component {

  render() {
    const {
      actions,
      auth,
    } = this.props;

    return (
      <main className="settings center">
        <Sign name="Settings" />

        <Reminders />

        <p>{_.get(auth, 'user.email')}</p>

        {
          _.get(auth, 'user.email') && !isLoading(auth.state) &&
          <button
            onClick={actions.logout}
          >
            Logout
        </button>
        }
      </main>
    );
  }
}

export default connect(
  state => {
    return {
      auth: state.auth,
    };
  },
  dispatch => {
    return {
      actions: bindActionCreators(authActions, dispatch),
    };
  }
)(Settings);