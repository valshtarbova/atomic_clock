import React, { Component } from 'react';
import _ from 'lodash';

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { timerActions } from 'redux/reducers/timerReducer';
import { projectsActions } from 'redux/reducers/projectsReducer';
import { tasksActions } from 'redux/reducers/tasksReducer';

// constants
import { isLoading, } from 'constants/loadingStates';

// utils
import timer from 'utils/timer';

// components
import Timer from 'components/Timer';
import Select from 'components/Select';
import Form from 'components/Form';
import Loader from 'components/Loader';
import Worker, { WORKER_STATES } from 'components/Worker';

class TimerPage extends Component {
  state = {
    selectedProject: null,
    selectedTask: null,
    workerState: WORKER_STATES.THINKING,
  }

  componentDidMount() {
    const { actions, } = this.props;
    actions.timer.getTimeLogs();
    actions.projects.getProjects();
  }

  componentDidUpdate() {
    const {
      currentLog,
    } = this.props;
    const {
      selectedProject,
      selectedTask,
    } = this.state;

    if (
      !selectedProject &&
      _.get(currentLog, 'type') === timer.STATES.START
    ) {
      this.selectProject(currentLog.project);
    }

    if (
      !selectedTask &&
      _.get(currentLog, 'type') === timer.STATES.START
    ) {
      this.selectTask(currentLog.task);
    }
  }

  selectProject = (project) => {
    const { actions, } = this.props;

    this.setState({
      selectedProject: project,
    });
    actions.tasks.getTasks(project.ref);
  }

  selectTask = (task) => {
    this.setState({
      selectedTask: task,
    });
  }

  startTimer = () => {
    const {
      actions,
      currentLog,
    } = this.props;
    const {
      selectedProject,
      selectedTask,
    } = this.state;

    if (selectedProject && selectedTask && _.get(currentLog, 'type') !== timer.STATES.START) {
      actions.timer.startTimer(selectedProject.ref, selectedTask);
    }
  }

  render() {
    const {
      actions,
      currentLog,
      timerState,
      projects,
      tasks,
      projectsState,
      tasksState,
      logsForDay,
    } = this.props;

    const {
      selectedProject,
      selectedTask,
      workerState,
    } = this.state;

    return (
      <main className="timerPage center">

        <Loader loading={isLoading(timerState)}>
          <Form className="row" onSubmit={this.startTimer}>
            <Loader loading={isLoading(projectsState)}>
              <Select
                label="Project"
                selector={project => project.name}
                disabled={_.get(currentLog, 'type') === timer.STATES.START}
                options={projects}
                value={selectedProject}
                onSelect={this.selectProject}
              />
            </Loader>
            <Loader loading={isLoading(tasksState)}>
              <Select
                label="Task"
                disabled={!selectedProject || _.get(currentLog, 'type') === timer.STATES.START}
                selector={task => task.name}
                onChange={({ query }) => this.selectTask({ name: query })}
                options={tasks}
                value={selectedTask}
                onSelect={this.selectTask}
              />
            </Loader>
          </Form>
        </Loader>

        <Worker
          state={_.get(currentLog, 'type') === timer.STATES.START ? workerState : WORKER_STATES.WAITING}
          progress={0} max={8}
        />

        <Loader loading={isLoading(timerState)}>
          <Timer
            onTick={(period) => {
              let newState = WORKER_STATES.THINKING;
              if (period.minutes < 1 && period.hours < 1) {
                newState = WORKER_STATES.THINKING;
              } else if (period.minutes < 10 && period.hours < 1) {
                newState = WORKER_STATES.TYPING;
              } else if (period.hours < 2) {
                newState = WORKER_STATES.WARMED_TYPING;
              } else if (period.hours < 4) {
                newState = WORKER_STATES.ZONED;
              } else if (period.hours < 7) {
                newState = WORKER_STATES.GODLIKE;
              } else if (period.hours < 8) {
                newState = WORKER_STATES.BREAKING_THE_LIMIT;
              } else if (period.hours >= 8) {
                newState = WORKER_STATES.OVERLOAD;
              }
              if (workerState !== newState) {
                this.setState({ workerState: newState });
              }
            }}
            state={_.get(currentLog, 'type')}
            logs={logsForDay}
            onStart={this.startTimer}
            onStop={() => {
              actions.timer.stopTimer(currentLog.projectRef, currentLog.taskRef);
            }}
            disabled={
              (!selectedProject || !selectedTask) &&
              _.get(currentLog, 'type') !== timer.STATES.START
            }
          />
        </Loader>

      </main>
    );
  }
}

export default connect(
  state => {
    const {
      currentLog,
      state: timerState,
      logsForDay,
    } = state.timer;

    const {
      projects,
      state: projectsState,
    } = state.projects;

    const {
      tasks,
      state: tasksState,
    } = state.tasks;

    return {
      currentLog,
      timerState,
      projects,
      projectsState,
      tasks,
      tasksState,
      logsForDay,
    };
  },
  dispatch => {
    return {
      actions: {
        timer: bindActionCreators(timerActions, dispatch),
        projects: bindActionCreators(projectsActions, dispatch),
        tasks: bindActionCreators(tasksActions, dispatch),
      }
    };
  }
)(TimerPage);