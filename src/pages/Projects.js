import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import {
  withRouter,
} from "react-router-dom";

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { projectsActions } from 'redux/reducers/projectsReducer';

// modals
import CreateProject from 'modals/CreateProject';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';

// components
import Loader from 'components/Loader';
import Icon from 'components/Icon';

class Projects extends Component {
  state = {
    createProjectVisible: false,
  }

  componentDidMount() {
    const { actions, } = this.props;
    actions.projects.getProjects();
  }

  selectProject = (project) => {
    const { history } = this.props;
    history.push(`project/${project.ref.id}`);
  }

  render() {
    const {
      projects,
      projectsState,
    } = this.props;
    const {
      createProjectVisible,
    } = this.state;

    return (
      <section className="projects">
        <Loader loading={projectsState === LOADINGSTATES.LOADING}>
          <table>
            <caption>Projects</caption>
            <thead>
              <tr>
                <th>Project Name</th>
                <th>Client</th>
                <th>Created Date</th>
                <th>Status</th>
                <th>
                  <Icon className="projects_icon" name="add_project"
                    onClick={() => {
                      this.setState({ createProjectVisible: true })
                    }}
                  />
                </th>
              </tr>
            </thead>
            <tbody>
              {
                projects.map(project => {
                  return (
                    <tr key={_.uniqueId()}>
                      <td>{_.get(project, 'name')}</td>
                      <td>{_.get(project, 'client.name')}</td>
                      <td>{moment(_.get(project, 'createdDate')).format("DD MMM YYYY")}</td>
                      <td>{_.get(project, 'status')}</td>
                      <td>
                        <Icon className="projects_icon" name="view"
                          onClick={() => this.selectProject(project)}
                        />
                      </td>
                    </tr>
                  );
                })
              }
            </tbody>
          </table>
        </Loader>
        <CreateProject visible={createProjectVisible} onClose={() => this.setState({ createProjectVisible: false })} />
      </section>
    );
  }
}

export default withRouter(connect(
  state => {
    const {
      projects,
      state: projectsState,
    } = state.projects;
    return {
      projects,
      projectsState,
    };
  },
  dispatch => {
    return {
      actions: {
        projects: bindActionCreators(projectsActions, dispatch),
      }
    };
  }
)(Projects));