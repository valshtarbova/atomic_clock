import React, { Component } from 'react';
import _ from 'lodash';
import {
  withRouter,
} from "react-router-dom";

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { usersActions } from 'redux/reducers/usersReducer';
import { permissionsGroupsActions } from 'redux/reducers/permissionsGroupsReducer';

// constants
import { isLoading } from 'constants/loadingStates';

import Loader from 'components/Loader';

class Users extends Component {
  componentDidMount() {
    const { actions, } = this.props;
    actions.users.getUsers();
    actions.permissionsGroups.getPermissionsGroups();
  }


  render() {
    const {
      users,
      usersState,
      permissionsGroups,
      permissionsGroupsState,
    } = this.props;

    return (
      <section className="users">
        <Loader loading={isLoading(usersState) && isLoading(permissionsGroupsState)}>
          <table>
            <caption>Users</caption>
            <thead>
              <tr>
                <th>Email</th>
                <th>Permissions</th>
              </tr>
            </thead>
            <tbody>
              {
                users.map(user => {
                  const permissionsGroup = _.size(user.permissions) > 0 ?
                    _.find(permissionsGroups, pg =>
                      _.isEqual(
                        _.sortBy(pg.permissions),
                        _.sortBy(user.permissions)
                      )
                    ) || { name: "custom" } : { name: 'unauthorized' };
                  return (
                    <tr key={_.uniqueId()}>
                      <td>{_.get(user, 'email')}</td>
                      <td>{permissionsGroup.name}</td>
                    </tr>
                  );
                })
              }
            </tbody>
          </table>
        </Loader>
      </section>
    );
  }
}

export default withRouter(connect(
  state => {
    const {
      users,
      state: usersState,
    } = state.users;
    const {
      permissionsGroups,
      state: permissionsGroupsState,
    } = state.permissionsGroups;
    return {
      users,
      usersState,
      permissionsGroups,
      permissionsGroupsState,
    };
  },
  dispatch => {
    return {
      actions: {
        users: bindActionCreators(usersActions, dispatch),
        permissionsGroups: bindActionCreators(permissionsGroupsActions, dispatch),
      }
    };
  }
)(Users));