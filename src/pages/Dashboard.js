import React, { Component } from 'react';

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { timerActions } from 'redux/reducers/timerReducer';

import Projects from 'pages/Projects';
import Users from 'pages/Users';

// utils
import timer from 'utils/timer';

// constants
import { isLoading, } from 'constants/loadingStates';

// components
import Sign from 'components/Sign';
import Loader from 'components/Loader';

class Dashboard extends Component {
  componentDidMount() {
    const { timerActions, } = this.props;
    timerActions.getTimeLogs();
  }

  render() {
    const {
      timerState,
      logsForDay,
      logsForWeek,
      logsForMonth,
    } = this.props;
    return (
      <main className="dashboard center">
        <Sign name="Dashboard" />

        <Loader loading={isLoading(timerState)}>
          <div className="column row-gt-xs dashboard__info">
            <div className="dashboard__info-block">Today: {timer.format(timer.calcTime(logsForDay))} </div>
            <div className="dashboard__info-block">Week: {timer.format(timer.calcTime(logsForWeek))} </div>
            <div className="dashboard__info-block">Month: {timer.format(timer.calcTime(logsForMonth))}</div>
          </div>
        </Loader>


        <div
          className={`
            dashboard__tables column flex-center
            row-gt-md flex-unset-gt-md space-around-gt-md flex-wrap-gt-md
          `}
        >
          <Projects />
          <hr className="self-stretch" />
          <Users />
        </div>
      </main>
    );
  }
}

export default connect(
  state => {
    const {
      state: timerState,
      logsForDay,
      logsForWeek,
      logsForMonth,
    } = state.timer;
    return {
      timerState,
      logsForDay,
      logsForWeek,
      logsForMonth,
    };
  },
  dispatch => {
    return {
      timerActions: bindActionCreators(timerActions, dispatch),
    };
  }
)(Dashboard);