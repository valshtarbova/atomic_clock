import React, { Component } from 'react';
import _ from 'lodash';

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { authActions } from 'redux/reducers/authReducer';

// constants
import { isLoading } from 'constants/loadingStates';

// components
import Sign from 'components/Sign';
import Loader from 'components/Loader';

class Auth extends Component {
  componentDidUpdate() {
    const {
      auth,
      history,
      location,
    } = this.props;

    if (auth.isAuth) {
      history.push(_.get(location, 'state.redirect') || '/');
    }
  }

  render() {
    const {
      actions,
      auth,
    } = this.props;

    return (
      <main className="auth center">
        <Sign name="Authenticate" />

        {
          !_.get(auth, 'user.email') &&
          <Loader loading={isLoading(auth.state)}>
            <button
              onClick={actions.login}
            >
              Login
          </button>
          </Loader>
        }
      </main>
    );
  }
}

export default connect(
  state => {
    return {
      auth: state.auth,
    };
  },
  dispatch => {
    return {
      actions: bindActionCreators(authActions, dispatch),
    };
  }
)(Auth);