import React, { Component } from 'react';
import _ from 'lodash';

// redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { projectsActions } from 'redux/reducers/projectsReducer';
import { tasksActions } from 'redux/reducers/tasksReducer';
import { timerActions } from 'redux/reducers/timerReducer';

// utils
import timer from 'utils/timer';

// constants
import { isLoading } from 'constants/loadingStates';

// components
import Loader from 'components/Loader';
import Sign from 'components/Sign';

class Project extends Component {
  componentDidMount() {
    const { actions, } = this.props;
    this.setProject();
    actions.projects.getProjects();
    actions.tasks.getTasks(_.get(this.project, 'ref'));
    actions.timer.getLogsForProject(_.get(this.project, 'ref'));
  }

  componentDidUpdate() {
    const { actions, } = this.props;
    this.setProject();
    actions.tasks.getTasks(_.get(this.project, 'ref'));
    actions.timer.getLogsForProject(_.get(this.project, 'ref'));
  }

  setProject = () => {
    const { projects, match, } = this.props;
    if (!this.project || !this.project.ref) {
      this.project = _.find(projects, project => project.ref.id === match.params.id) || {};
    }
  }

  selectTask = (task) => {

  }

  getTimeForTask = (task) => {
    const {
      logsForProject,
    } = this.props;

    let time = 0;

    if (_.size(logsForProject) > 0 && task) {
      const logsForTask = _.filter(logsForProject, log => log.taskRef.id === task.ref.id);
      const users = _.groupBy(logsForTask, 'userRef.id')
      _.forEach(users, (logs) => {
        time += timer.calcTime(logs);
      })
    }

    return timer.format(time);
  }

  render() {
    const {
      tasks,
      tasksState,
      projectsState,
      history,
      match,
      logsForProject,
      logsForProjectState,
    } = this.props;

    if (!_.get(match, 'params.id')) {
      history.push(`/dashboard`);
    }

    const users = _.groupBy(logsForProject, 'userRef.id');
    _.forEach(users, (logs, key) => {
      users[key] = timer.calcTime(logs);
    });

    this.setProject();

    return (
      <main className="project center">
        <Sign name={this.project.name}
        // onBackClicked={()=> history.push(`/dashboard`)}
        // subcontent={
        //   <Loader loading={isLoading(logsForProjectState, tasksState, projectsState)}>
        //     <div className="column">
        //       <div>Users: {_.size(users)}</div>
        //       <div>Tasks: {_.size(tasks)}</div>
        //       <div>
        //         Time: {
        //           timer.format(_.reduce(users, (acc, val) => acc + val))
        //         }
        //       </div>
        //     </div>
        //   </Loader>
        // }
        />

        <Loader loading={isLoading(logsForProjectState, tasksState, projectsState)}>
          <div className="column row-gt-xs">
            <div className="project__info-block">Users: {_.size(users)}</div>
            <div className="project__info-block">Tasks: {_.size(tasks)}</div>
            <div className="project__info-block">
              Time: {
                timer.format(_.reduce(users, (acc, val) => acc + val))
              }
            </div>
          </div>
        </Loader>

        <div
          className={`
            project__tables column flex-center
            row-gt-sm flex-unset-gt-sm space-around-gt-sm flex-wrap-gt-sm
          `}
        >
          <Loader loading={isLoading(logsForProjectState, projectsState)}>
            <table>
              <caption>Users</caption>
              <thead>
                <tr>
                  <th>Email</th>
                  <th>Time</th>
                </tr>
              </thead>
              <tbody>
                {
                  _.map(users, (time, email) => {
                    return (
                      <tr key={_.uniqueId()}>
                        <td>{email}</td>
                        <td>{timer.format(time)}</td>
                      </tr>
                    );
                  })
                }
              </tbody>
            </table>
          </Loader>
          <hr className="self-stretch" />
          <Loader loading={isLoading(logsForProjectState, tasksState, projectsState)}>
            <table>
              <caption>Tasks</caption>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Time</th>
                </tr>
              </thead>
              <tbody>
                {
                  tasks.map((task, key) => {
                    return (
                      <tr key={_.uniqueId()} onClick={() => this.selectTask(task)}>
                        <td>{key + 1}</td>
                        <td>{task.name}</td>
                        <td>{this.getTimeForTask(task)}</td>
                      </tr>
                    );
                  })
                }
              </tbody>
            </table>
          </Loader>
        </div>

      </main >
    );
  }
}

export default connect(
  state => {
    const {
      projects,
      state: projectsState,
    } = state.projects;
    const {
      tasks,
      state: tasksState,
    } = state.tasks;
    const {
      logsForProject,
      logsForProjectState,
    } = state.timer;
    return {
      tasks,
      tasksState,
      projects,
      projectsState,
      logsForProject,
      logsForProjectState,
    };
  },
  dispatch => {
    return {
      actions: {
        projects: bindActionCreators(projectsActions, dispatch),
        tasks: bindActionCreators(tasksActions, dispatch),
        timer: bindActionCreators(timerActions, dispatch),
      }
    };
  }
)(Project);