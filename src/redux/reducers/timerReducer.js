import _ from 'lodash';
import moment from 'moment';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';
import { REDUCERS } from 'constants/reducers';

import createReducer from 'redux/createReducer';

import { tasksRef } from 'utils/firebaseService';
import { timeLogsRef } from 'utils/firebaseService';
import timer from 'utils/timer';

const defaultState = {
  currentLog: null,
  logs: null,
  logsForMonth: [],
  logsForWeek: [],
  logsForDay: [],
  logsForProject: null,
  logsForProjectRef: null,
  logsForProjectListener: null,
  state: LOADINGSTATES.INIT,
  logsForProjectState: LOADINGSTATES.INIT,
};

export const { state: timerState, actions: timerActions } = createReducer({
  name: REDUCERS.TIMER,
  defaultState,
  actions: {
    getTimeLogs: () => ({ modify, getState, }) => {
      const state = getState();
      const user = state.auth.user;

      if (!_.get(user, 'ref') || _.get(state, 'timer.logs')) {
        return null;
      }

      if (_.get(state, 'timer.listener')) {
        _.get(state, 'timer.listener')();
      }
      const listener = timeLogsRef.where("userRef", "==", user.ref)
        .orderBy("createdDate", "desc")
        .onSnapshot(docs => {
          const logs = [];
          docs.forEach(doc => {
            const event = doc.data();
            event.createdDate = event.createdDate.toDate();
            event.ref = doc.ref;
            logs.push(event)
          });
          const currentLog = _.first(logs);
          const logsForMonth = getLogsForPeriod(logs, 'month');
          const logsForWeek = getLogsForPeriod(logsForMonth, 'week');
          const logsForDay = getLogsForPeriod(logsForWeek, 'day');
          getLogData(currentLog).then(log => {
            modify({
              state: LOADINGSTATES.LOADED,
              logs,
              currentLog: log,
              logsForDay,
              logsForWeek,
              logsForMonth,
            });
          })
        });
      modify({ state: LOADINGSTATES.LOADING, listener, });

    },

    getLogsForProject: (projectRef) => ({ modify, getState, }) => {
      const state = getState();
      const user = state.auth.user;

      if (
        !_.get(user, 'ref') ||
        !projectRef ||
        _.get(state, 'timer.logsForProjectRef') === projectRef
      ) {
        return null;
      }

      if (_.get(state, 'timer.logsForProjectListener')) {
        _.get(state, 'timer.logsForProjectListener')();
      }
      const logsForProjectListener = timeLogsRef.where("projectRef", "==", projectRef)
        .onSnapshot(docs => {
          const logs = [];
          docs.forEach(doc => {
            const event = doc.data();
            event.createdDate = event.createdDate.toDate();
            event.ref = doc.ref;
            logs.push(event)
          });
          modify({ logsForProjectState: LOADINGSTATES.LOADED, logsForProject: _.orderBy(logs, ['createdDate'], ['desc']), });
        });
      modify({ logsForProjectState: LOADINGSTATES.LOADING, logsForProjectListener, logsForProjectRef: projectRef });
    },

    startTimer: (projectRef, task) => ({ modify, getState, }) => {
      const state = getState();
      modify({ state: LOADINGSTATES.LOADING, });
      if (task && !task.ref) {
        const id = projectRef.id + '-' + task.name;
        tasksRef.doc(id).set({
          id,
          name: task.name,
          projectRef,
        }).then(() => {
          toggleTimer({
            state,
            type: timer.STATES.START,
            modify,
            projectRef,
            taskRef: tasksRef.doc(id),
          });
        })
      } else if (task && task.ref) {
        toggleTimer({
          state,
          type: timer.STATES.START,
          modify,
          projectRef,
          taskRef: task.ref,
        });
      }
    },

    stopTimer: (projectRef, taskRef) => ({ modify, getState, }) => {
      const state = getState();
      modify({ state: LOADINGSTATES.LOADING, });
      toggleTimer({
        state,
        type: timer.STATES.STOP,
        modify,
        projectRef,
        taskRef,
      });
    },

  },
});

function toggleTimer({
  type,
  state,
  modify,
  projectRef,
  taskRef,
}) {
  const user = _.get(state, 'auth.user.ref');
  if (!user || !projectRef || !taskRef) {
    return modify({ state: LOADINGSTATES.ERROR });
  }

  const event = {
    createdDate: new Date(),
    userRef: user,
    type,
    projectRef,
    taskRef,
  };

  timeLogsRef.add(event).then(res => {
    console.log('done', res);
  }).catch(err => {
    console.log('err', err);
  });
}

async function getLogData(event) {
  if (!event) {
    return event;
  }

  if (event.projectRef) {
    const project = await event.projectRef.get();
    event.project = project.data();
    event.project.ref = event.projectRef;
  }

  if (event.taskRef) {
    const task = await event.taskRef.get();
    event.task = task.data();
    event.task.ref = event.taskRef;
  }

  return event;
}

function getLogsForPeriod(logs, period) {
  const _logs = logs.filter(
    log =>
      moment(log.createdDate).format("YYYYDDD") >=
      moment().startOf(period).format("YYYYDDD")
  );

  if (_.size(_logs) === 0 && _.get(_.first(logs), 'type') === timer.STATES.START) {
    _logs.push(_.first(logs));
  }

  return _logs;
}