import _ from 'lodash';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';
import { REDUCERS } from 'constants/reducers';

import createReducer from 'redux/createReducer';

import { projectStatusRef } from 'utils/firebaseService';

const defaultState = {
  projectStatus: [],
  state: LOADINGSTATES.INIT,
};

export const { state: projectStatusState, actions: projectStatusActions } = createReducer({
  name: REDUCERS.PROJECT_STATUS,
  defaultState,
  actions: {
    getProjectStatus: () => ({ modify, getState, }) => {
      const state = getState();
      const user = state.auth.user;

      //TODO: check users' permissions
      if (
        !_.get(user, 'ref') ||
        _.size(_.get(state, 'projectStatus.projectStatus'))
      ) {
        return;
      }

      projectStatusRef.get()
        .then(docs => {
          const projectStatus = [];
          docs.forEach(doc => {
            const status = doc.data();
            status.ref = doc.ref;
            projectStatus.push(status);
          });
          modify({ state: LOADINGSTATES.LOADED, projectStatus, });
        });
      modify({ state: LOADINGSTATES.LOADING, });

    },

  },
});
