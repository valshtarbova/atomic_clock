import _ from 'lodash';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';
import { REDUCERS } from 'constants/reducers';

import createReducer from 'redux/createReducer';

import { remindersRef } from 'utils/firebaseService';

import { subscribeNotification, unsubscribeNotification } from 'utils/notificationService';

const defaultState = {
  reminders: [],
  remindersState: LOADINGSTATES.INIT,
  saveReminderState: LOADINGSTATES.INIT,
};

export const { state: remindersState, actions: remindersActions } = createReducer({
  name: REDUCERS.REMINDERS,
  defaultState,
  actions: {
    getReminders: () => ({ modify, getState, }) => {
      const state = getState();
      const user = state.auth.user;

      if (!_.get(user, 'ref')) {
        return;
      }

      if (_.get(state, 'tasks.listener')) {
        _.get(state, 'tasks.listener')();
      }

      const listener = remindersRef.where('userRef', '==', user.ref)
        .onSnapshot(docs => {
          const reminders = [];
          docs.forEach(doc => {
            const reminder = doc.data();
            reminder.ref = doc.ref;
            reminder.date = reminder.date.toDate();
            reminders.push(reminder);
            subscribeNotification(reminder);
          });
          modify({ remindersState: LOADINGSTATES.LOADED, reminders, });
        });
      modify({ remindersState: LOADINGSTATES.LOADING, listener, });

    },

    deleteReminder: reminder => ({ modify }) => {
      modify({ remindersState: LOADINGSTATES.LOADING, });
      unsubscribeNotification(reminder);
      reminder.ref.delete();
    },

    saveReminder: ({
      id,
      name,
      date,
    }) => ({ modify, getState, }) => {
      if (!id || !name || !date) {
        return;
      }

      modify({ saveReminderState: LOADINGSTATES.LOADING, });

      const state = getState();
      const user = state.auth.user;

      if (!_.get(user, 'ref')) {
        return;
      }

      remindersRef.doc(id).set({
        id,
        name,
        date,
        userRef: user.ref,
      }).then(() => {
        modify({ saveReminderState: LOADINGSTATES.LOADED, });
      }).catch(() => {
        modify({ saveReminderState: LOADINGSTATES.ERROR, });
      });
    },
  },

});
