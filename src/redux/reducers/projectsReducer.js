import _ from 'lodash';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';
import { REDUCERS } from 'constants/reducers';

import createReducer from 'redux/createReducer';

import { projectsRef } from 'utils/firebaseService';

const defaultState = {
  projects: [],
  state: LOADINGSTATES.INIT,
  createProjectState: LOADINGSTATES.INIT,
};

export const { state: projectsState, actions: projectsActions } = createReducer({
  name: REDUCERS.PROJECTS,
  defaultState,
  actions: {
    getProjects: () => ({ modify, getState, }) => {
      const state = getState();
      const user = state.auth.user;

      if (_.size(state.projects.projects)) {
        return;
      }

      //TODO: check users' permissions
      if (!_.get(user, 'ref')) {
        return null;
      }

      if (_.get(state, 'projects.listener')) {
        _.get(state, 'projects.listener')();
      }

      const listener = projectsRef
        .onSnapshot(docs => {
          const promises = [];

          docs.forEach(doc => {
            promises.push((async () => {
              const project = doc.data();
              project.ref = doc.ref;
              project.createdDate = project.createdDate.toDate();

              if (project.clientRef) {
                const client = await project.clientRef.get()
                project.client = client.data();
              }

              return project;
            })());
          })

          Promise.all(promises).then((projects) => {
            modify({ state: LOADINGSTATES.LOADED, projects, });
          });
        });
      modify({ state: LOADINGSTATES.LOADING, listener, });

    },

    createProject: ({ name, clientRef, status, }) => ({ modify }) => {
      if (!name || !clientRef || !status) {
        return;
      }
      modify({ createProjectState: LOADINGSTATES.LOADING, });
      const shortName = name.replace(/[a-z]/g, '');
      projectsRef.doc(shortName).set({
        id: shortName,
        name,
        clientRef,
        status,
        createdDate: new Date(),
      }).then(() => {
        modify({ createProjectState: LOADINGSTATES.LOADED, });
      }).catch(() => {
        modify({ createProjectState: LOADINGSTATES.ERROR, });
      });
    }
  },
});
