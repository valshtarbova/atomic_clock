import _ from 'lodash';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';
import { REDUCERS } from 'constants/reducers';

import createReducer from 'redux/createReducer';

import { clientsRef } from 'utils/firebaseService';

const defaultState = {
  clients: [],
  state: LOADINGSTATES.INIT,
  listener: null,
};

export const { state: clientsState, actions: clientsActions } = createReducer({
  name: REDUCERS.CLIENTS,
  defaultState,
  actions: {
    getClients: () => ({ modify, getState, }) => {
      const state = getState();
      const user = state.auth.user;

      //TODO: check users' permissions
      if (!_.get(user, 'ref')) {
        return;
      }


      if (_.get(state, 'clients.listener')) {
        _.get(state, 'clients.listener')();
      }

      const listener = clientsRef
        .onSnapshot(docs => {
          const clients = [];
          docs.forEach(doc => {
            const client = doc.data();
            client.ref = doc.ref;
            clients.push(client);
          });
          modify({ state: LOADINGSTATES.LOADED, clients, });
        });
      modify({ state: LOADINGSTATES.LOADING, listener, });

    },

  },
});
