import _ from 'lodash';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';
import { REDUCERS } from 'constants/reducers';

import createReducer from 'redux/createReducer';

import { tasksRef } from 'utils/firebaseService';

const defaultState = {
  tasks: [],
  tasksForProject: null,
  state: LOADINGSTATES.INIT,
};

export const { state: tasksState, actions: tasksActions } = createReducer({
  name: REDUCERS.TASKS,
  defaultState,
  actions: {
    getTasks: (projectRef) => ({ modify, getState, }) => {
      const state = getState();
      const user = state.auth.user;

      //TODO: check users' permissions
      if (!_.get(user, 'ref')) {
        return;
      }
      if (!projectRef || state.tasks.tasksForProject === projectRef.id) {
        return;
      }

      if (_.get(state, 'tasks.listener')) {
        _.get(state, 'tasks.listener')();
      }

      const listener = tasksRef.where("projectRef", "==", projectRef)
        .onSnapshot(docs => {
          const tasks = [];
          docs.forEach(doc => {
            const task = doc.data();
            task.ref = doc.ref;
            tasks.push(task);
          });
          modify({ state: LOADINGSTATES.LOADED, tasks, tasksForProject: projectRef.id });
        });
      modify({ state: LOADINGSTATES.LOADING, listener, tasksForProject: null, });

    },

  },
});
