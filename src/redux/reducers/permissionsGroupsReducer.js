import _ from 'lodash';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';
import { REDUCERS } from 'constants/reducers';

import createReducer from 'redux/createReducer';

import { permissionsGroupsRef } from 'utils/firebaseService';

const defaultState = {
  permissionsGroups: [],
  state: LOADINGSTATES.INIT,
};

export const { state: permissionsGroupsState, actions: permissionsGroupsActions } = createReducer({
  name: REDUCERS.PERMISSIONS_GROUPS,
  defaultState,
  actions: {
    getPermissionsGroups: () => ({ modify, getState, }) => {
      const state = getState();
      const user = state.auth.user;

      //TODO: check users' permissions
      if (!_.get(user, 'ref')) {
        return;
      }

      permissionsGroupsRef.get()
        .then(docs => {
          const permissionsGroups = [];
          docs.forEach(doc => {
            const permissionsGroup = doc.data();
            permissionsGroups.push(permissionsGroup);
          });
          modify({ state: LOADINGSTATES.LOADED, permissionsGroups, });
        });
      modify({ state: LOADINGSTATES.LOADING, });

    },

  },
});
