// constants
import { LOADINGSTATES } from 'constants/loadingStates';
import { REDUCERS } from 'constants/reducers';

import createReducer from 'redux/createReducer';

import firebase, { usersRef } from 'utils/firebaseService';

const defaultState = {
  user: null,
  isAuth: false,
  state: LOADINGSTATES.INIT,
};

export const { state: authState, actions: authActions } = createReducer({
  name: REDUCERS.AUTH,
  defaultState,
  actions: {
    authUser: () => ({ modify }) => {
      modify({ state: LOADINGSTATES.LOADING, });
      firebase.auth().onAuthStateChanged(googleUser => {
        if (googleUser) {
          getUser({ user: googleUser, modify });
        } else {
          modify({ state: LOADINGSTATES.LOADED, user: null, isAuth: false });
        }
      });
    },

    login: () => ({ modify }) => {
      modify({ state: LOADINGSTATES.LOADING, });

      const provider = new firebase.auth.GoogleAuthProvider();
      // provider.addScope('https://www.googleapis.com/auth/contacts.readonly');

      firebase.auth().signInWithPopup(provider).then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        // const token = result.credential.accessToken;
        // The signed-in user info.
        const googleUser = result.user;
        getUser({ user: googleUser, modify, });
        // ...
      }).catch((error) => {
        // Handle Errors here.
        // const errorCode = error.code;
        // const errorMessage = error.message;
        // The email of the user's account used.
        // const email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        // const credential = error.credential;
        modify({ state: LOADINGSTATES.ERROR, });
      });
    },

    logout: () => ({ modify }) => {
      modify({ state: LOADINGSTATES.LOADING, });
      firebase.auth().signOut();
    },
  }
});

function getUser({ user, modify }) {
  usersRef.doc(user.email).get().then((doc) => {
    if (doc.exists) {
      const _user = doc.data();
      _user.ref = doc.ref;
      _user.photoURL = user.photoURL;
      modify({ state: LOADINGSTATES.LOADED, user: _user, isAuth: true });
    } else {
      addUser({ user, modify, });
    }
  }).catch((error) => {
    console.log("Error getting user: ", { error });
    modify({ state: LOADINGSTATES.ERROR, });
  });
}

function addUser({ user, modify }) {
  const ref = usersRef.doc(user.email);
  const _user = {
    email: user.email,
    createdDate: new Date(),
    id: user.email,
    permissions: [],
  };
  ref.set(_user).then(() => {
    _user.ref = ref;
    _user.photoURL = user.photoURL;
    modify({ state: LOADINGSTATES.LOADED, user: _user, isAuth: true });
  }).catch((error) => {
    console.log("Error adding user: ", { error });
    modify({ state: LOADINGSTATES.ERROR, });
  });
}