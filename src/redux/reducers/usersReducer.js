import _ from 'lodash';

// constants
import { LOADINGSTATES } from 'constants/loadingStates';
import { REDUCERS } from 'constants/reducers';

import createReducer from 'redux/createReducer';

import { usersRef } from 'utils/firebaseService';

const defaultState = {
  users: [],
  state: LOADINGSTATES.INIT,
};

export const { state: usersState, actions: usersActions } = createReducer({
  name: REDUCERS.USERS,
  defaultState,
  actions: {
    getUsers: () => ({ modify, getState, }) => {
      const state = getState();
      const user = state.auth.user;

      if (_.size(state.users.users)) {
        return;
      }

      //TODO: check users' permissions
      if (!_.get(user, 'ref')) {
        return null;
      }

      if (_.get(state, 'users.listener')) {
        _.get(state, 'users.listener')();
      }

      const listener = usersRef
        .onSnapshot(docs => {
          const users = [];
          docs.forEach(async doc => {
            const user = doc.data();
            user.ref = doc.ref;
            user.createdDate = user.createdDate.toDate();
            users.push(user);
          });

          modify({ state: LOADINGSTATES.LOADED, users, });
        });
      modify({ state: LOADINGSTATES.LOADING, listener, });

    },
  },
});
