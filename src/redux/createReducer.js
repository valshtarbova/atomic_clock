import _ from 'lodash';

export default ({ name: type, defaultState, actions, }) => {
  const type_id = _.uniqueId(type);
  const temp = {};
  _.forEach(actions, (action, actionName) => {
    temp[actionName] = (...params) => (dispatch, getState) =>
      // pass down the params
      action(...params)({
        // pass down standart dispatch and getState
        dispatch,
        getState,
        // pass down modifier for the reduer
        // use unique type_id to make sure that only this reducer is modified
        modify: state => dispatch({ type: type_id, state, }),
      });
  });

  return {
    state: (state = defaultState, action) => {
      if (action.type === type || action.type === type_id) {
        return { ...state, ...action.state };
      }
      return state;
    },

    actions: temp,
  };
}

