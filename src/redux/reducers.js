import { combineReducers } from "redux";

import { authState } from "redux/reducers/authReducer";
import { timerState } from "redux/reducers/timerReducer";
import { projectsState } from "redux/reducers/projectsReducer";
import { clientsState } from "redux/reducers/clientsReducer";
import { projectStatusState } from "redux/reducers/projectStatusReducer";
import { tasksState } from "redux/reducers/tasksReducer";
import { usersState } from "redux/reducers/usersReducer";
import { permissionsGroupsState } from "redux/reducers/permissionsGroupsReducer";
import { remindersState } from "redux/reducers/remindersReducer";

export default combineReducers({
  auth: authState,
  timer: timerState,
  projects: projectsState,
  clients: clientsState,
  projectStatus: projectStatusState,
  tasks: tasksState,
  users: usersState,
  permissionsGroups: permissionsGroupsState,
  reminders: remindersState,
});
