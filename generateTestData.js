const admin = require('firebase-admin');
const moment = require('moment');
const uuid = require('uuid');

const serviceAccount = require('./serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

const testData = {};

testData.project_status = getStatus(["Completed", "InProgress"]);
testData.clients = getClients(["Transmatico", "Atom"]);
testData.projects = getProjects(["WaterForSale", "AtomicClock"]);
testData.tasks = getTasks();
testData.users = [
  {
    id: "veselinivanov1994@gmail.com",
    createdDate: randDate(),
    email: "veselinivanov1994@gmail.com",
    permissions: ["view_projects", "create_project"],
  },
  {
    id: "v.shtarbova1415@gmail.com",
    createdDate: randDate(),
    email: "v.shtarbova1415@gmail.com",
    permissions: ["view_projects", "create_project", "view_users"],
  },
  {
    id: "vei@atom.com",
    createdDate: randDate(),
    email: "vei@atom.com",
    permissions: [],
  },
  {
    id: "val@atom.com",
    createdDate: randDate(),
    email: "val@atom.com",
    permissions: ["view_projects", "create_project"],
  },
];
testData.time_logs = getLogs();
testData.permissions = getPermissions(["view_projects", "create_project"]);
testData.permissions_groups = [
  {
    id: "user",
    name: "user",
    permissions: ["view_projects"],
  },
  {
    id: "PM",
    name: "PM",
    permissions: ["view_projects", "create_project"],
  },

];

testData.reminders = getReminders(["Remember to Sleep", "Drink water"]);

function getReminders(names) {
  const user = randFromList(testData.users);
  return names.map(name => ({
    id: uuid(),
    name,
    date: randDate(),
    userRef: db.collection('users').doc(user.id),
  }));
}

function getPermissions(names) {
  return names.map(name => ({
    id: name,
    name,
  }));
}

function getStatus(names) {
  return names.map(name => ({
    id: name,
    name,
  }));
}

function getClients(names) {
  return names.map(name => ({
    id: name,
    name,
    active: randBool(),
    createdDate: randDate(),
  }));
}

function getProjects(names) {
  return names.map(name => ({
    id: name.replace(/[a-z]/g, ''),
    clientRef: db.collection('clients').doc(randFromList(testData.clients).id),
    createdDate: randDate(),
    name,
    status: randFromList(testData.project_status).name,
  }));
}

function getTasks() {
  const tasks = [];

  testData.projects.forEach(project => {
    const shortName = project.id + "-";
    for (let index = 0; index < randNumber(6, 24); index++) {
      tasks.push({
        id: shortName + index,
        name: shortName + index,
        projectRef: db.collection('projects').doc(project.id),
      });
    }
  });

  return tasks;
}

function getLogs() {
  const logs = [];
  testData.users.forEach(user => {
    const count = randNumber(3, 10);
    for (let index = 1; index < count; index++) {
      const project = randFromList(testData.projects);
      const projectRef = db.collection('projects').doc(project.id);
      const task = randFromList(testData.tasks.filter(task => task.projectRef.id === projectRef.id));
      const taskRef = db.collection('tasks').doc(task.id);
      const userRef = db.collection('users').doc(user.id);
      const date = moment().subtract(count, 'days').hours(randNumber(7, 10)).date(index);
      logs.push({
        createdDate: date.toDate(),
        projectRef,
        taskRef,
        type: "START",
        userRef,
        id: uuid(),
      });
      logs.push({
        createdDate: date.add(randNumber(3, 8), 'hours').toDate(),
        projectRef,
        taskRef,
        type: "STOP",
        userRef,
        id: uuid(),
      });
    }
  });

  return logs;
}

function randNumber(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randFromList(list) {
  if (list && list.length) {
    return list[randNumber(0, list.length - 1)];
  }
}

function randDate() {
  return new Date(
    Date.now() - randNumber(0, 365) * 24 * 60 * 60 * 1000
  );
}

function randBool() {
  return !!randNumber(0, 1);
}

function generateTestData() {
  // Get a new write batch
  const batch = db.batch();

  // generate batch from testData
  Object.keys(testData).forEach(key => {
    const items = testData[key];
    items.forEach(item => {
      const ref = db.collection(key).doc(item.id);
      batch.set(ref, item);
    });
  });

  // Commit the batch
  batch.commit().then(() => {
    console.log('done batch', testData);
  });
}

generateTestData();