## Getting Started
Run the scripts in the project diretory:
### `npm install`
### `npm start`


## Available Scripts

In the project directory, you can run:

### `npm install`

Install all the necessary packages.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run deploy`

Builds the app for production and deploys it to firebase

### `node ./generateTestData`

Generates test data to cloud firestore.
You need to provide serviceAccountKey.json in the root directory of the project.
You can get the file from https://console.cloud.google.com/iam-admin/serviceaccounts.
Also FirebaseConfig inside ./utils/firebaseService.js need to be change.
